"""
A class representing an Agent.

Author: Robert-Mihai Botarleanu
"""

from actions import Actions


class Agent:

    def __init__(self, name, position, recipes, plan=None, commands=None):
        if commands is None:
            commands = []
        if plan is None:
            plan = []

        self.name = name  # name of the agent
        self.position = position  # current known platform
        self.commands = commands
        self.recipes = recipes

        self.total_payoff = 0  # current 'score'
        self.waiting_for_message = False  # waiting for a message

        self.plan_completed = False  # agent is done
        self.received_message = False  # agent has received a message

    def perceive(self, percepts):
        messages = percepts.inbound_messages
        self.position = percepts.position
        previous_action_successful = percepts.previous_action_successful
        if self.waiting_for_message and len(messages) > 0:
            self.waiting_for_message = False
            self.received_message = True

    def get_action(self):
        """ Returns the first action in the plan. """
        if len(self.plan) == 0:
            return None
        return self.plan.pop(0)

    def get_intention(self):
        """ Returns the second action in the plan. """
        if len(self.plan) < 2:
            return None
        return self.plan[1]

    def act(self, percepts):
        """ Returns an action, given the percepts. """
        self.perceive(percepts)
        action = None

        if self.waiting_for_message:
            return Actions.WaitAction()

        # Get the next action in the plan
        action = self.get_action()
        if action is None:  # agent is done if plan list is empty
            self.plan_completed = True
            return None

        # If the agent has received a Wait Action
        if isinstance(action, Actions.WaitAction):
            # Check if any messages have been received in perceptions
            if len(percepts.inbound_messages) == 0:
                # Will wait for messages
                self.waiting_for_message = True
                return Actions.WaitAction()
            else:
                # Will carry on with the plan
                return self.act(percepts)

        # Next means the agent is done for this turn
        if isinstance(action, Actions.NextAction):
            return None

        # Standard action (finalize, moveTo etc)
        return action

    def get_state(self):
        """ Prints the intention. """
        if not self.plan_completed:
            self.intention = self.get_intention()
            return '%s: @%s; intention: %s' % (str(self), str(self.position),
                                               str(self.intention))
        else:
            return '%s: @%s; plan completed' % (str(self), str(self.position))

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return hash(self.name)