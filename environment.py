"""
A class representing the Environment.

The environment is composed of agents and platforms and its role is to manage
all actions performed by these entities, in a discrete time universe.

Author: Robert-Mihai Botarleanu
"""

from actions import Actions
from percepts import Percepts
from time import sleep


class Environment:

    def __init__(self, N, M, TF, platforms, agents, platform_connections, sleep_time):
        """
        Parameters
        ----------
        N: int
            the number of agents
        M: int
            the number of platforms
        TF: int
            the execution stop time
        platform_connections: list
            a list of edges in the undirected graph
        sleep: int
            time (im millis) that the system sleeps between time moments
        """
        self.N = N
        self.M = M
        self.TF = TF
        self.platform_connections = platform_connections
        self.platforms = platforms
        self.agents = agents

        self.agent_positions = {}
        self.messages = {}
        self.previous_action_success = {}
        self.work_in_progress = {}

        self.t = 0  # current time
        self.init_wm()

        self.locked_agents = set()

        self.to_execute = []

        self.sleep_time = sleep_time / 1000

    def is_action_instant(self, action):
        """ Returns true if the action can be executed instantly. """
        return isinstance(action, Actions.FinalizeAction) or \
               isinstance(action, Actions.MessageToAction) or \
               isinstance(action, Actions.WaitAction)

    def query_agents(self):
        """ Queries agents for this turn. """

        # agents that are locked through a non-instant action
        self.locked_agents = set()

        # agents that have said they are done for this turn
        finished_agents = set()

        # run until all agents say they are done for the turn
        while len(finished_agents) != len(self.agents):
            for agent in self.agents:
                # ignore finished agents
                if agent in finished_agents:
                    continue

                # interract with the agents
                percepts = self.build_agent_percepts(agent)
                action = agent.act(percepts)

                # execute action if instant
                if self.is_action_instant(action):
                    success = self.execute_instant_action(agent, action)
                    self.previous_action_success[agent] = success
                else:
                    # if agent says it's done, carry on
                    if action is None:
                        finished_agents.add(agent)
                        continue

                    # check non-instant actions and queue them
                    success = self.check_validity(agent, action)
                    self.previous_action_success[agent] = success

                    # process does not block the agent, since it's a platform
                    # activity
                    if not isinstance(action, Actions.ProcessAction):
                        self.locked_agents.add(agent)

    def run_simulation(self):
        """ Runs the system for TF steps. """
        for self.t in range(self.TF + 1):
            print()
            print('=' * 15 + ' t-%d' % self.t)
            self._reset_messagebox()
            # Check platforms for process finishes
            self.poll_platforms()
            # Print agent intentions
            self.print_agent_states()
            # Query agents
            self.query_agents()
            if self.t + 1 <= self.TF:
                print('-' * 15 + ' t-%d -> t-%d' % (self.t, self.t + 1))
                # Execute non-instant actions
                self.execute_queued_up_actions()
                # Print platform states at end of turn
                self.print_platform_states()
            sleep(self.sleep_time)
        print('== END.')

    def execute_queued_up_actions(self):
        """ Executes any non-instant actions from this turn. """
        for agent, action in self.to_execute:
            if isinstance(action, Actions.MoveToAction):
                # Change agent position
                print('%s moving %s -> %s' % (str(agent),
                                              str(agent.position),
                                              str(action.platform)))
                self.agent_positions[agent] = action.platform
            if isinstance(action, Actions.ProcessAction):
                command = action.command
                capability = action.capability
                platform = agent.position

                # Get recipe for process
                for recipe in agent.recipes:
                    if recipe.capability == capability:
                        agent_recipe = recipe
                        break

                # Tell the platform to execute the process with the recipe
                platform.execute_recipe(agent_recipe)
                self.work_in_progress[platform] = (agent, command, capability)

                # Command is no longer with agent
                if command in agent.commands:
                    agent.commands.remove(command)

                # Process is no longer required for command
                command.procs.remove(capability)
            if isinstance(action, Actions.TransferToAction):
                command = action.command
                target_agent = action.agent

                # Move command to new agent
                agent.commands.remove(command)
                target_agent.commands.append(command)

                print('@%s transferring %s %s -> %s' % (
                    str(agent.position), str(command), str(agent),
                    str(target_agent)
                ))

        # Clear the queued up actions list
        self.to_execute = []

    def execute_instant_action(self, agent, action):
        """ Handles an instant action. """
        if isinstance(action, Actions.MessageToAction):
            return self.handle_messageToAction(action, agent)

        if isinstance(action, Actions.FinalizeAction):
            return self.check_finalize_action(action, agent)

        if isinstance(action, Actions.WaitAction):
            # Wait actions require no environment response
            return True

    def check_finalize_action(self, action, agent):
        """ Returns True if action is valid, or False otherwise. """
        command = action.command
        print('%s -> finalize: %s' % (str(agent), str(command)), end='')

        if action.command not in agent.commands:
            print(' - Agent does not have the command in its possession!')
            return False
        if action.command.agent != agent:
            print(' - Agent is not the initial agent!')
            return False
        if len(action.command.procs) != 0:
            print(' - Not all processes have been completed!')
            return False
        print()

        # Compute payoff
        if self.t < command.deadline:
            payoff = command.payoff
        else:
            payoff = max(0, command.payoff - command.penalty *
                         (self.t - command.deadline))
        # Add payoff to agent 'score'
        agent.total_payoff += payoff

        print('%s <- payoff %d' % (str(agent), payoff))
        # Command is finished, remove from the agents' list.
        agent.commands.remove(command)

        return True

    def handle_messageToAction(self, action, agent):
        """ Returns True if action is valid, or False otherwise. """
        print('%s -> %s: %s' % (str(agent),
                                str(action.agent),
                                str(action.message)),
              end='')

        if self.agent_positions[agent] != \
                self.agent_positions[action.agent]:
            print(' - Agents not on the same platform!')
            return False

        print()
        self.messages[action.agent].append(action.message)
        return True

    def check_move_action(self, agent, action):
        """ Returns True if action is valid, or False otherwise. """
        platform = action.platform
        print('%s -> moveTo %s' % (str(agent), str(platform)), end='')

        if agent in self.locked_agents:
            print(' - Agent is already executing another '
                  'non-instant action')
            return False

        if platform not in self.platforms:
            print(' - Invalid platform!')
            return False
        if (agent.position, platform) not in self.platform_connections:
            print(' - No connection between platforms!')
            return False

        print()
        self.to_execute.append((agent, action))
        return True

    def check_process_action(self, agent, action):
        """ Returns True if action is valid, or False otherwise. """
        capability = action.capability
        command = action.command

        print('%s -> process %s %s' % (str(agent),
                                       str(capability),
                                       str(command)),
              end='')

        if command not in agent.commands:
            print(' - Agent does not have that command!')
            return False

        if capability != command.procs[0]:
            print(" - Process is not at the head "
                  "of the commands' procs!")
            return False

        # Find if the agent has a recipe for the process
        agent_recipe = None
        for recipe in agent.recipes:
            if recipe.capability == capability:
                agent_recipe = recipe
                break

        if agent_recipe is None:
            print(' - Agent does not have a recipe for that capability!')
            return False

        platform = self.agent_positions[agent]

        if platform.busy:
            print(' - Platform is busy!')
            return False

        if capability not in platform.capabilities:
            print(' - Platform does not have that capability!')
            return False

        print()
        self.to_execute.append((agent, action))
        return True

    def check_transferTo_action(self, agent, action):
        """ Returns True if action is valid, or False otherwise. """
        command = action.command
        transfer_agent = action.agent

        print('%s -> transferTo %s %s' % (str(agent), str(transfer_agent),
                                          str(command)), end='')

        if self.agent_positions[agent] != self.agent_positions[transfer_agent]:
            print(' Agents not on same platform!')
            return False

        if command not in agent.commands:
            print(' %s is not in posession of %s at this time.' % (
                str(agent), str(command)
            ))
            return False

        self.to_execute.append((agent, action))

        print()
        return True

    def check_validity(self, agent, action):
        if isinstance(action, Actions.NextAction):
            return True
        if isinstance(action, Actions.MoveToAction):
            return self.check_move_action(agent, action)
        if isinstance(action, Actions.ProcessAction):
            return self.check_process_action(agent, action)
        if isinstance(action, Actions.TransferToAction):
            return self.check_transferTo_action(agent, action)

    def print_platform_states(self):
        """ Prints the busy platforms. """
        for platform in self.platforms:
            if platform.busy:
                agent, command, process = self.work_in_progress[platform]
                print('%s: process %s %s %s (done t-%d)' % (
                    str(platform), str(process),
                    str(command), str(agent),
                    platform.time_done
                ))

    def poll_platforms(self):
        """ Asks platforms if they have finished processing. """
        for platform in self.platforms:
            if platform.poll(self.t):
                # Platform has finished a process
                agent, command, process = self.work_in_progress[platform]
                print('%s <- %s' % (str(agent), str(command)), end='')
                del self.work_in_progress[platform]

                if self.agent_positions[agent] is not platform:
                    print(' - Agent not at platform to receive '
                          'the processed command!')
                    continue

                print()
                agent.commands.append(command)

    def _reset_messagebox(self):
        for agent in self.agents:
            self.messages[agent] = []

    def init_wm(self):
        """ Sets-up environment variables for agent handling. """
        for agent in self.agents:
            self.agent_positions[agent] = agent.position
            self.previous_action_success[agent] = None

    def build_agent_percepts(self, agent):
        """ Builds perceptions for the agent. """
        agent_messages = self.messages[agent]
        self.messages[agent] = []
        return Percepts(agent_messages,
                        self.agent_positions[agent],
                        self.t,
                        self.TF,
                        self.previous_action_success[agent])

    def print_agent_states(self):
        """ Prints the intentions of the agents. """
        for agent in self.agents:
            agent.perceive(self.build_agent_percepts(agent))
            print(agent.get_state())
