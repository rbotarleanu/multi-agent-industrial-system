"""
A class representing an inter-agent message.

Author: Robert-Mihai Botarleau
"""


class Message:

    def __init__(self, sender, receiver, msg):
        """
        Parameters
        ----------
        sender: agent
        receciver: agent
        msg: object
        """

        self.sender = sender
        self.receiver = receiver
        self.msg = msg
    
    def __str__(self):
        return '[Message: %s->%s: %s]' % (str(self.sender),
                                          str(self.receiver),
                                          str(self.msg))
