"""
A class representing a recipe.

A recipe is a <capability, required time> tuple.

Author: Robert-Mihai Botarleanu
"""


class Recipe:

    def __init__(self, capability, required_time):
        """
        capability: Capability
            the capability
        required_time: int
            ammount of time required to process this capability
        """
        self.capability = capability
        self.required_time = required_time

    def __str__(self):
        return "Recipe(%s, dt: %f)" % (
            str(self.capability),
            self.required_time
        )
