"""
Class representing Agent perceptions.
"""


class Percepts:

    def __init__(self,
                 inbound_messages,
                 position,
                 timestamp,
                 TF,
                 previous_action_successful=True):
        """
        inbound_messages: list
            list of messages the agent receives
        position: platform
            the platform of the agent (known by env)
        timestamp: int
            the current time
        TF: int
            agents should know the TF
        previous_action_successful: boolean
            true if previous action succeeded
        """
        self.inbound_messages = inbound_messages
        self.position = position
        self.previous_action_successful = previous_action_successful
        self.timestamp = timestamp
        self.TF = TF
