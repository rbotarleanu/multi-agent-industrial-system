"""
The class that parses the input and starts the simulation.

Author: Robert-Mihai Botarleanu
"""

import sys
from actions import Actions
from agent import Agent
from capability import Capability
from command import Command
from environment import Environment
from platform import Platform
from recipe import Recipe


READ_PLANS = True  # set to True if agents receive pre-made plans
DEBUG = False  # set to True for testfile parsing debug prints
SLEEP = 2000  # 2000 ms


def next_line(fin):
    """ Returns the next non-comment and non-empty line. """
    while True:
        line = fin.readline().strip()
        if len(line) > 0 and '//' not in line:
            break

    return line


def parse_input_and_run(testfile):
    """ Primary function to parse a testfile. """
    with open(testfile, 'rt') as fin:
        # Read first line
        N, M, TF, NCOM = read_header(fin)

        # Read the edges
        edges = read_edges(fin)

        # Read the platform capabilities
        platforms = read_platforms(M, fin)

        # Convert edge nodes to the actual platforms (by name)
        edges = convert_platform_names_to_platforms(edges, platforms)

        # Read the agent information
        agents = read_agents(N, fin, platforms)

        # Read the commands
        commands = read_commands(NCOM, agents, fin)

        # Read the plans (if requested)
        if READ_PLANS:
            read_plans(N, agents, commands, fin, platforms)

        env = Environment(N, M, TF, platforms, agents, edges, SLEEP)
    env.run_simulation()


def read_plans(N, agents, commands, fin, platforms):
    for agent_index in range(N):
        plan = []
        line = next_line(fin)
        agent = agents[agent_index]
        while True:
            if len(line) == 0:  # next agent plan
                break
            if '//' in line:
                line = fin.readline().strip()
                continue
            line = line.split()

            # Read the next command in the plan
            action_name = line[0]

            parse_action(action_name, agent, agents,
                         commands, line, plan, platforms)

            line = fin.readline().strip()

        agents[agent_index].plan = plan


def parse_action(action_name, current_agent, agents,
                 commands, line, plan, platforms):
    if action_name == 'wait':
        plan.append(Actions.WaitAction())
    if action_name == 'next':
        plan.append(Actions.NextAction())
    if action_name == 'moveTo':
        for p in platforms:
            if p.name == line[1]:
                plan.append(Actions.MoveToAction(p))
                break
    if action_name == 'finalize':
        for c in commands:
            if c.name == line[1]:
                plan.append(Actions.FinalizeAction(c))
                break
    if action_name == 'process':
        cap = Capability(line[1])
        for c in commands:
            if c.name == line[2]:
                plan.append(Actions.ProcessAction(cap, c))
                break
    if action_name == 'messageTo':
        for a in agents:
            if a.name == line[1]:
                agent = a
                break
        msg = ' '.join(line[2:])
        plan.append(Actions.MessageToAction(agent, msg))
    if action_name == 'transferTo':
        for a in agents:
            if a.name == line[1]:
                agent = a
                break
        for c in commands:
            if c.name == line[2]:
                command = c
                break
        plan.append(Actions.TransferToAction(agent, command))


def read_commands(NCOM, agents, fin):
    commands = []
    for _ in range(NCOM):
        line = next_line(fin).split()
        com_name = line[0]
        for agent in agents:
            if agent.name == line[1]:
                com_agent = agent
                break

        deadline = int(line[2])
        reward = float(line[3])
        penalty = float(line[4])

        procs = []
        for i in range(5, len(line)):
            procs.append(Capability(line[i]))
        commands.append(Command(com_name, com_agent, deadline, reward, penalty, procs))
        # Assign the command to the agent
        com_agent.commands.append(commands[-1])
    if DEBUG:
        print('After reading commands')
        print('\n'.join([str(c) for c in commands]))
        print('\n'.join([str(a) for a in agents]))
        print()

    return commands


def read_agents(N, fin, platforms):
    agents = []
    for _ in range(N):
        line = next_line(fin).split()
        agent_name = line[0]

        # Find the platform with the correct name
        for platform in platforms:
            if platform.name == line[1]:
                agent_platform = platform
                break

        # Parse the recipes
        recipes = []
        for i in range(2, len(line) - 1, 2):
            recipe_cap = Capability(line[i])
            recipe_dt = float(line[i + 1])
            recipes.append(Recipe(recipe_cap, recipe_dt))
        agents.append(Agent(agent_name, agent_platform, recipes))
    if DEBUG:
        print('Agents')
        print('\n'.join([str(a) for a in agents]))
        print()

    return agents


def read_header(fin):
    line = next_line(fin).split()
    N, M, TF, NCOM = line
    N, M, TF, NCOM = int(N), int(M), int(TF), int(NCOM)
    if DEBUG:
        print(N, M, TF, NCOM)
    return N, M, TF, NCOM


def read_edges(fin):
    line = next_line(fin).split()
    edges = []
    for i in range(0, len(line) - 1, 2):
        edges.append((line[i], line[i + 1]))
        edges.append((line[i+1], line[i]))  # undirected graph
    if DEBUG:
        print(edges)
    return edges


def read_platforms(M, fin):
    platforms = []
    for _ in range(M):
        line = next_line(fin).split()
        pname = line[0]
        platform_capabilities = []
        for cap in line[1:]:
            platform_capabilities.append(Capability(cap))
        platforms.append(Platform(pname, platform_capabilities))
    if DEBUG:
        print('Platforms')
        print(' '.join([str(p) for p in platforms]))
        print()
    return platforms


def convert_platform_names_to_platforms(edges, platforms):
    new_edges = []
    for (n1, n2) in edges:
        platform1 = None
        platform2 = None
        for platform in platforms:
            if platform.name == n1:
                platform1 = platform
            if platform.name == n2:
                platform2 = platform
        new_edges.append((platform1, platform2))
    if DEBUG:
        print('Edges')
        print('\n'.join(['%s -> %s' % (n1, n2) for (n1, n2) in new_edges]))
        print()

    return new_edges


if __name__ == '__main__':
    testfile = 'system.txt'
    if len(sys.argv) > 2:
        raise Exception('Usage: tester [file]')
    if len(sys.argv) == 2:
        testfile = sys.argv[1]
    if DEBUG:
        print('Running with testfile: %s' % testfile)

    parse_input_and_run(testfile)
