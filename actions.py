"""
A class encapsulating the actions an agent can take.

Author: Robert-Mihai Botarleanu
"""

class Actions:

    class GenericAction:
        pass

    class MoveToAction(GenericAction):
        def __init__(self, platform):
            self.platform = platform

        def __str__(self):
            return 'moveTo %s' % str(self.platform)

    class ProcessAction(GenericAction):
        def __init__(self, capability, command):
            self.capability = capability
            self.command = command

        def __str__(self):
            return 'process %s %s' % (str(self.command), str(self.capability))

    class FinalizeAction(GenericAction):
        def __init__(self, command):
            self.command = command

        def __str__(self):
            return 'finalize %s' % str(self.command)

    class MessageToAction(GenericAction):
        def __init__(self, agent, message):
            self.agent = agent
            self.message = message

        def __str__(self):
            return '%s -> %s' % (str(self.message), str(self.agent))

    class TransferToAction(GenericAction):
        def __init__(self, agent, command):
            self.agent = agent
            self.command = command

        def __str__(self):
            return 'transfer %s to %s' % (str(self.command), str(self.agent))

    class NextAction(GenericAction):
        def __init__(self):
            pass

        def __str__(self):
            return 'next'

    class WaitAction(GenericAction):
        def __init__(self):
            pass

        def __str__(self):
            return 'wait'