"""
A class representing a capability.id

Author: Robert-Mihai Botarleanu
"""


class Capability:

    def __init__(self, name):
        """
        Parameters
        ----------
        name: str
            the name of the platform (an identifier, e.g. P1)
        """

        if type(name) is not str:
            raise TypeError('A capability name should be a string.')

        self.name = name

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return hash(self.name)
