"""
A class representing a platform.

Author: Robert-Mihai Botarleanu
"""


class Platform:
    
    def __init__(self, name, capabilities):
        """
        name: str
            the name of the platform
        capabilities: list
            list of platform capabilities
        """
        self.name = name
        self.capabilities = capabilities
        self.busy = False  # True if executing a capability
        self.process = None  # Process that is being executed
        self.time_done = None  # When the process will be completed
        self.t = 0  # Last known timestamp

    def execute_recipe(self, recipe):
        """ Starts executing a process after a recipe.  """
        self.busy = True
        self.process = recipe.capability
        self.time_done = self.t + recipe.required_time

    def poll(self, t):
        """
        Returns true if the platform has finished processing something.
        Will change the state to not busy if a process is completed at
        this time.
        """
        self.t = t
        if not self.busy:
            return False

        if self.t == self.time_done:
            self.busy = False
            return True

        return False

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return hash(self.name)
