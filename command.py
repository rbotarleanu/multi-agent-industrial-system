"""
A class representing a command.

A command is a 5-tuple
    <agent, procs, deadline, payoff, penalty>.


Author: Robert-Mihai Botarleanu
"""


class Command:

    def __init__(self, name, agent, deadline, payoff, penalty, procs):
        """
        Parameters
        ----------
        name: str
            the name of the command (e.g. com1)
        agent: Agent instance
            the agent that initially received the command 
        procs: list of Capability instances
            the command preconditions
        deadline: int
            the maximum time allowed before penalties are incurred
        payoff: float
            the payoff that *agent* receives if the deadline is met
        penalty: float
            for each moment of time that the deadline is exceeded, the penalty
            that will be subtracted from the payoff.
        """
        self.name = name
        self.agent = agent
        self.procs = procs
        self.deadline = deadline
        self.payoff = payoff
        self.penalty = penalty

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return hash(self.name)
